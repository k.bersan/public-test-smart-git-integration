# Public Test Smart Git Integration

Public Test Smart Git Integration

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<link rel="stylesheet" type="text/css" href="styles/style.css"/>
</head>
<body class="opacity">
<div class="cv_main_container">
	<div class="cv_header">
		<div id="headshot" style="float: left;">
			<img src="file:///Home/Downloads/IMG_5638.jpeg"/ width="128" height="128">
		</div>
		<div class="information" style="float: left;">
			<div id="name">
				<h2 class="quickFade delayTwo">Kateryna Bersan</h2>
				<h2 class="quickFade delayThree">Junior Quality Assurance Engineer</h2>
				<h2><a class="li" href="mailto:savchenkokateryna0904@gmail.com">savchenkokateryna0904@gmail.com</a> </h2>
					<h2><a class="li" href="https://www.linkedin.com/in/kateryna-savchenko-6a4890145/" target="_blank">LinkedIn</a></h2>
			</div>
		</div>
	</div>
	<div class="cv">
		<h1>Summary:</h1>
		<p>I am a 26 year old. After having worked for several years at the University, I have decided to make a switch in my career. I have long since dreamed of a career in IT and I now pursuing this dream. In the last seven months, I have passed an English language certification at the University of Amsterdam and online software testing training courses, Selenium IDE,  HTML and CSS courses and most important - I have three months experience in the dutch IT company “PressPage”.</p>
	</div>
	<div class="wh">
			<h1>Work History:</h1>
			<div class="work container">
			<table>
			<tbody>
				<tr>
					<td class="date">May 2017 Aug 2017</td>
					<td class="description">Junior Quality Assurance Engineer ”PressPage”, Amsterdam;</td>
				</tr>
				<tr>
					<td class="date">Sep 2014 Sep 2016</td>
					<td class="description">Teaching Assistant Odessa International Humanitarian University, Department of civil and family law, Odessa;</td>
				</tr>
				<tr>
					<td class="date">Nov 2012 Sep 2016</td>
					<td class="description">Laborant Odessa International Humanitarian University, Department of civil and family law, Odessa;</td>
				</tr>
				<tr>
					<td class="date">Jun 2012 Aug 2012</td>
					<td class="description">Sociological Researcher The Cherkassy city Council;</td>
				</tr>
				<tr>
					<td class="date">Dec 2009 May 2012</td>
					<td class="description">Promoter “Фокстрот”,”Волошкове поле”,”Алан”.</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="education container">
		<h1>Education:</h1>
		<table>
			<tbody>
				<tr>
					<td class="date">Sep 2015 Jun 2016</td>
					<td class="description">Odessa International Humanitarian University,
						Postgraduate education of Civil,
						Intellectual Property and Family law;
					</td>
				</tr>
				<tr>
					<td class="date">Sep 2009 Jun 2014</td>
					<td class="description">National University “Odessa Law Academy”,
						Faculty of Advocates, Master's degree.
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="courses container">
			<h1>Course and Certifications:</h1>
			<table>
			<tbody>
				<tr>
					<td class="date">Jul 2017 Present</td>
					<td class="description">Technology Nation student, Brainbasket;</td>
				</tr>
				<tr>
					<td class="date">Jun 2017 Present</td>
					<td class="description">HTML,CSS, Self-Study on Codecademy, W3school;</td>
				</tr>
					<td class="date">Mar 2017 May 2017</td>
					<td class="description">Selenium IDE, Self-Study on Udemy resources;</td>
				</tr>
				<tr>
					<td class="date">Jan 2017 Feb 2017</td>
					<td class="description">Theory Testing, Self-Study on Udemy resources;</td>
				</tr>
				<tr>
					<td class="date">Dec 2016</td>
					<td class="description">English Language Course, University of Amsterdam (UVA talen),
					 Intermediate level.</td>
				</tr>
				</tbody>
		</table>
	</div>
	<div class="ts container">
			<h1>Technical skills:</h1>
				<ul>
					<li>Git Hub, Php Storm, Sublime Text, Source Tree, Kitematic (Beta), Docker;</li>
					<li>Jira, Confluence;</li>
					<li>Postman; Swagger Editor; Selenium IDE;</li>
					<li>Basic knowledge: HTML, CSS;</li>
					<li>I had already worked with Scrum;</li>
					<li>Testing Documentation skills;</li>
					<li>Testing types: Manual, Automation,Web, Cross browser, Regression.</li>
				</ul>
	</div>
	<div class="ls container">
			<h1>Language skills:</h1>
				<ul>
					<li>Ukrainian: Mother tongue;</li>
					<li>Russian: Advanced;</li>
					<li>English: Upper intermediate.</li>
				</ul>
	</div>
	<div class="int container">
			<h1>Interest:</h1>
					<p>Running, writing, animals.</p>
	</div>
</div>
</body>
</html>
